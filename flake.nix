{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/23.11";
    treefmt-nix.url = "github:numtide/treefmt-nix";
  };

  outputs = { self, nixpkgs, treefmt-nix, systems }:
    let
      eachSystem = f:
        nixpkgs.lib.genAttrs (import systems)
        (system: f nixpkgs.legacyPackages.${system});
      treefmtEval =
        eachSystem (pkgs: treefmt-nix.lib.evalModule pkgs ./treefmt.nix);
    in {
      # Use `nix fmt`
      formatter =
        eachSystem (pkgs: treefmtEval.${pkgs.system}.config.build.wrapper);

      # Use `nix flake check`
      checks = eachSystem (pkgs: {
        formatting = treefmtEval.${pkgs.system}.config.build.check self;
      });

      # Use `nix develop`
      devShells = eachSystem (pkgs: {
        default = let pkgs = import nixpkgs { system = "x86_64-linux"; };
        in pkgs.mkShell {
          packages = with pkgs; [ nim nimPackages.nimble treefmt nixfmt ];

          shellHook = ''
            export PS1="\n[nix-shell:\w]$ "
          '';
        };
      });
    };
}
