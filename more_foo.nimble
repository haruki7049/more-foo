# Package

version       = "0.1.0"
author        = "haruki7049"
description   = "more-foo"
license       = "MIT"
srcDir        = "src"
bin           = @["more_foo"]


# Dependencies

requires "nim >= 1.6.14"
