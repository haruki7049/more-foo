# more-foo
もっとFooと言おう！！ `-t`オプションは`Foo`と言う回数を示します。

使い方
```bash
mfoo
# Foo!!
```
```bash
mfoo -t 2
# FooFoo!!
```
